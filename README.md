# Frequency domain electromagnetic induction: multi-coil vs multi-frequency instruments

This work compares multi-coils and multi-frequencies FDEM instruments with synthetic and field measurements. It serves as a companion to the submitted manuscript:

Blanchy et al. (submitted) Comparison of multi-coil and multi-frequency frequency domain electromagnetic induction instruments.

Submitted to the special issue: "[Digital soil mapping using electromagnetic sensors](https://www.frontiersin.org/research-topics/50481/digital-soil-mapping-using-electromagnetic-sensors)" of Frontiers


